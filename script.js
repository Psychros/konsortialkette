/** 
 * @param {org.example.empty.erstelleKredit} erstelleKredit
 * @transaction
 */
async function OnCreateCredit(erstelleKredit){
  console.log('erstelleKredit')
  
  const factory = getFactory();
  const NS = 'org.example.empty';
  
  if(erstelleKredit.hoehe <= 0)
    throw new Error('Hoehe muss groesser als 0 sein!');
  
  if(erstelleKredit.laufzeit <= 0)
    throw new Error('Laufzeit muss groesser als 0 sein!');
  
  if(erstelleKredit.maxZinssatz < erstelleKredit.minZinssatz)
    throw new Error('Max. Zinssatz darf nicht kleiner als MinZinssatz sein!');
  
  const kredit = factory.newResource(NS, 'Kredit', erstelleKredit.id);
  kredit.id = erstelleKredit.id;
  kredit.bank = factory.newRelationship(NS, 'Bank', erstelleKredit.bank);
  kredit.hoehe = erstelleKredit.hoehe;
  kredit.laufzeit = erstelleKredit.laufzeit;
  kredit.deadline = erstelleKredit.deadline;
  kredit.minZinssatz = erstelleKredit.minZinssatz
  kredit.maxZinssatz = erstelleKredit.maxZinssatz
  kredit.offen = True;
}


/** 
 * @param {org.example.empty.angebotAbgeben} angebotAbgeben
 * @transaction
 */
async function OnAngebotAbgeben(angebotAbgeben){
  console.log('angebotAbgeben')
  
  const factory = getFactory();
  const NS = 'org.example.empty';
  
  if(angebotAbgeben.hoehe <= 0)
    throw new Error('Hoehe muss groesser als 0 sein!');
  
  if(angebotAbgeben.zinssatz <= 0)
    throw new Error('Zinssatz muss groesser als 0 sein!');
  
  const angebot = factory.newResource(NS, 'Angebot', angebotAbgeben.id);
  angebot.id = angebotAbgeben.id;
  angebot.geldgeber = factory.newRelationship(NS, 'Bank', angebotAbgeben.bank);
  angebot.hoehe = angebotAbgeben.hoehe;
  angebot.zinssatz = angebotAbgeben.zinssatz;
  angebot.kredit = angebotAbgeben.kredit;
  angebot.angenommen = False;
  angebot.gegenangebot = False;
}

/** 
 * @param {org.example.empty.angebotAnnehmen} angebotAnnehmen
 * @transaction
 */
async function OnAngebotAnnehmen(angebotAnnehmen){
  console.log('angebotAnnehmen')
  
  const factory = getFactory();
  const NS = 'org.example.empty';
  
  //Angebot als abgeschlossen markieren
  angebotAnnehmen.angebot.angenommen = True;
  
  //Beteiligung erstellen
  const beteiligung = factory.newResource(NS, 'Beteiligung', angebotAnnehmen.angebot.id);
  beteiligung.zinssatz = angebotAnnehmen.angebot.zinssatz;
  beteiligung.hoehe = angebotAnnehmen.angebot.hoehe;
  beteiligung.geldgeber = factory.newRelationship(NS, 'Bank', angebotAnnehmen.angebot.geldgeber);

  angebotAnnehmen.angebot.kredit.beteiligungen.push(beteiligung);
}