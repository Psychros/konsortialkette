/** 
 * @param {org.example.empty.erstelleKredit} erstelleKredit
 * @transaction
 */
async function OnCreateCredit(erstelleKredit){
  console.log('erstelleKredit')
  
  const factory = getFactory();
  const NS = 'org.example.empty';
  
  if(erstelleKredit.hoehe <= 0)
    throw new Error('Hoehe muss groesser als 0 sein!');
  
  if(erstelleKredit.laufzeit <= 0)
    throw new Error('Laufzeit muss groesser als 0 sein!');
  
  if(erstelleKredit.maxZinssatz < erstelleKredit.minZinssatz)
    throw new Error('Max. Zinssatz darf nicht kleiner als MinZinssatz sein!');
  
  const kredit = factory.newResource(NS, 'Kredit', erstelleKredit.id);
  kredit.bank = factory.newRelationship(NS, 'Bank', erstelleKredit.bank.bankID);
  kredit.hoehe = erstelleKredit.hoehe;
  kredit.beteiligungen = [];
  kredit.laufzeit = erstelleKredit.laufzeit;
  kredit.deadline = erstelleKredit.deadline;
  kredit.minZinssatz = erstelleKredit.minZinssatz;
  kredit.maxZinssatz = erstelleKredit.maxZinssatz;
  kredit.offen = true;
  
  const kregistry = await getAssetRegistry(NS + '.Kredit');
  await kregistry.add(kredit);
}


/** 
 * @param {org.example.empty.angebotAbgeben} angebotAbgeben
 * @transaction
 */
async function OnAngebotAbgeben(angebotAbgeben){
  console.log('angebotAbgeben')
  
  const factory = getFactory();
  const NS = 'org.example.empty';
  
  if(angebotAbgeben.hoehe <= 0)
    throw new Error('Hoehe muss groesser als 0 sein!');
  
  if(angebotAbgeben.zinssatz <= 0)
    throw new Error('Zinssatz muss groesser als 0 sein!');
  
  const angebot = factory.newResource(NS, 'Angebot', angebotAbgeben.id);
  angebot.geldgeber = factory.newRelationship(NS, 'Bank', angebotAbgeben.geldgeber.bankID);
  angebot.hoehe = angebotAbgeben.hoehe;
  angebot.zinssatz = angebotAbgeben.zinssatz;
  angebot.kredit = angebotAbgeben.kredit;
  angebot.angenommen = false;
  angebot.gegenangebot = false;

  const aregistry = await getAssetRegistry(NS + '.Angebot');
  await aregistry.add(angebot);
}

/** 
 * @param {org.example.empty.angebotAnnehmen} angebotAnnehmen
 * @transaction
 */
async function OnAngebotAnnehmen(angebotAnnehmen){
  console.log('angebotAnnehmen')
  
  const factory = getFactory();
  const NS = 'org.example.empty';
  
  //Angebot als abgeschlossen markieren
  angebotAnnehmen.angebot.angenommen = true;
  
  //Beteiligung erstellen
  const beteiligung = factory.newResource(NS, 'Beteiligung', angebotAnnehmen.angebot.id);
  beteiligung.zinssatz = angebotAnnehmen.angebot.zinssatz;
  beteiligung.hoehe = angebotAnnehmen.angebot.hoehe;
  beteiligung.geldgeber = factory.newRelationship(NS, 'Bank', angebotAnnehmen.angebot.geldgeber.bankID);

  const bregistry = await getAssetRegistry(NS + '.Beteiligung');
  await bregistry.add(beteiligung);

  //Aktualisiere das aktuelle Angebot
  angebotAnnehmen.angebot.kredit.beteiligungen.push(beteiligung);

  //Aktualisiere das Budget der Bank
  angebotAnnehmen.angebot.geldgeber.geldmenge -= beteiligung.hoehe;

  const kregistry = await getAssetRegistry(NS + '.Kredit');
  await kregistry.update(angebotAnnehmen.angebot.kredit);

  const bankregistry = await getParticipantRegistry(NS + '.Bank');
  await bankregistry.update(angebotAnnehmen.angebot.geldgeber);

  const aregistry = await getAssetRegistry(NS + '.Angebot');
  await aregistry.update(angebotAnnehmen.angebot);

  //Prüfen, ob der Kredit geschlosen werden muss
  /*const value = 0;
  for(var bet in angebotAnnehmen.angebot.kredit.beteiligungen){
     value += bet.hoehe;
  }
  if(value == angebotAnnehmen.angebot.kredit.hoehe)
    OnCloseCredit()*/
}

/** 
 * @param {org.example.empty.angebotAblehnen} angebotAblehnen
 * @transaction
 */
async function OnAngebotAblehnen(angebotAblehnen){
  console.log('angebotAblehnen')
  const NS = 'org.example.empty';

  const aregistry = await getAssetRegistry(NS + '.Angebot');
  await aregistry.remove(angebotAblehnen.angebot);
}

/** 
 * @param {org.example.empty.kreditSchliessen} kreditSchliessen
 * @transaction
 */
async function OnKreditSchliessen(kreditSchliessen){
  console.log('kreditSchliessen')
  
  const factory = getFactory();
  const NS = 'org.example.empty';
  

  //Aktualisiere das Budget der Bank
  kreditSchliessen.kredit.bank.geldmenge += kreditSchliessen.kredit.hoehe;

  //Kredit schliessen
  kreditSchliessen.kredit.offen = false;

  const kregistry = await getAssetRegistry(NS + '.Kredit');
  await kregistry.update(kreditSchliessen.kredit);

  const bankregistry = await getParticipantRegistry(NS + '.Bank');
  await bankregistry.update(kreditSchliessen.kredit.bank);
}

